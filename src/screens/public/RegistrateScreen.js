import React, { useState } from "react";
import { View, Button, StyleSheet, Text, TextInput, TouchableOpacity} from "react-native";
import { MaterialIcons } from '@expo/vector-icons';

export default function RegistrateScreen(props){
    const {navigation} = props;

    const [nombre, setNombre] = useState('');
    const [documento, setDocumento] = useState('');
    const [telefono, setTelefono] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [setComfirmpassword, setComfirmPassword] = useState('');
    const [error, setError] = useState('');
    const [passwordVisible, setPasswordVisible] = useState(false);

    const togglePasswordVisibility = () => {
        setPasswordVisible(!passwordVisible);
    };

    const goToIni = () => {
        navigation.navigate("Inicio de Sesión");
      };
      
    return(
        <View style={styles.container}>
           <View style={styles.formContainer}>
                <Text style={styles.title}>Registro</Text>
                <TextInput
                    style={styles.input}
                    placeholder="Nombre"
                    onChangeText={(text) => setNombre(text)}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Documento"
                    onChangeText={(text) => setDocumento(text)}
                />
                 <TextInput
                    style={styles.input}
                    placeholder="Telefono"
                    onChangeText={(text) => setTelefono(text)}
                />
                 <TextInput
                    style={styles.input}
                    placeholder="Email"
                    onChangeText={(text) => setEmail(text)}
                />
                 
                <View style={styles.passwordContainer}>
                    <TextInput
                        style={styles.passwordInput}
                        placeholder="Contraseña"
                        secureTextEntry={!passwordVisible}
                        onChangeText={(text) => setPassword(text)}
                    />

                    <TouchableOpacity onPress={togglePasswordVisibility} style={styles.eyeIcon}>
                        <MaterialIcons name={passwordVisible ? 'visibility-off' : 'visibility'} size={24} color="black" />
                    </TouchableOpacity>
                </View>
                <View style={styles.passwordContainer}>
                <TextInput
                        style={styles.passwordInput}
                        placeholder="Confirmar contraseña"
                        secureTextEntry={!passwordVisible}
                        onChangeText={(text) => setComfirmPassword(text)}
                    />
                    <TouchableOpacity onPress={togglePasswordVisibility} style={styles.eyeIcon}>
                        <MaterialIcons name={passwordVisible ? 'visibility-off' : 'visibility'} size={24} color="black" />
                    </TouchableOpacity>
               </View>
                
               
               <TouchableOpacity style={styles.btn} onPress={goToIni}>
                    <Text style={styles.btnText}>Registrarse</Text>
                </TouchableOpacity>
                
            </View>
            
        </View>

    )
}

const styles = StyleSheet.create({
    formContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '115%',
        paddingHorizontal: 20, // Alinea el formulario con el ancho de la pantalla
        marginTop: -100, // Espacio entre la imagen y el formulario
        marginBottom:2,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 20,

    },
    title: {
        fontSize: 35,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 30,
        padding: 8,
        marginTop:10,
        
    },
    input: {
        height: 45,
        width: '90%',
        borderWidth: 1,
        marginBottom: 20,
        paddingHorizontal: 10,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#BBC3BF',

    },
    btn: {
        backgroundColor: "#4caf50",
        borderRadius: 17,
        paddingVertical: 10,
        width: "90%",
        height: "5%",
    },
    btnText: {
        color: "white", // Cambia el color del texto en el botón
        fontWeight: "bold",
        textAlign: "center",
        fontSize: 16,
    },

    containerLink: {
        alignSelf: "flex-end",
        marginTop: -20,
        marginBottom: 20,

    },
    links: {
        color: "#4caf50",
        alignSelf: "flex-end",
        marginBottom: 10,
        fontSize: 15,
        marginTop: 8,
    },
    passwordContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '90%',
        borderWidth: 1,
        borderRadius: 8,
        borderColor: '#BBC3BF',
        marginBottom: 20,
    },
    passwordInput: {
        flex: 1,
        height: 45,
        paddingHorizontal: 10,

    },
    eyeIcon: {
        padding: 10,
    },
    logo: {
        width: '100%', // Ocupa la mitad del ancho del contenedor
        height: '60%', // Ocupa la mitad del alto del contenedor
        resizeMode: 'cover',
        marginBottom:-150,
    },


});
