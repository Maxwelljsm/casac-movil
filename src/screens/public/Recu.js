import { View, Text, StyleSheet } from 'react-native';
import React, { useState } from 'react';
import { Icon, Input, Button } from 'react-native-elements';
/* import { getAuth, sendPasswordResetEmail } from 'firebase/auth';
import { getFirestore, collection, getDocs, query, where } from 'firebase/firestore'; */

export default function Recu(props) {
  const [email, setEmail] = useState("");
  const [showMessage, setShowMessage] = useState(false);
  const [promotoresData, setPromotoresData] = useState(null);

  /* const getPromotoresData = async () => {
    try {
      const firestore = getFirestore();
      const promotoresCollection = collection(firestore, 'promotor');
      const promotoresSnapshot = await getDocs(promotoresCollection);

      const data = [];
      promotoresSnapshot.forEach((doc) => {
        data.push(doc.data());
      });

      setPromotoresData(data);
      console.log('Datos de la colección de promotores:', data);
    } catch (error) {
      console.error('Error al obtener los datos de la colección de promotores', error);
    }
  }; */

  c/* onst sendPasswordReset = async () => {
    try {
      if (!promotoresData) {
        console.log('Primero se obtienen los datos de la colección de promotores.');
        return;
      }

      const auth = getAuth();
      const emailLowerCase = email.toLowerCase();

      // Buscar el correo electrónico en los datos de promotores
      const promotor = promotoresData.find((promotor) => promotor.correo === emailLowerCase);

      if (promotor) {
        // Si el usuario existe, enviar el correo de recuperación de contraseña
        await sendPasswordResetEmail(auth, emailLowerCase);

        console.log('Correo de recuperación enviado con exito....');
        setShowMessage(true); // Muestra el mensaje de éxito
      } else {
        console.log('El correo electrónico no se encuentra registrado......');
        setShowMessage(false); // Muestra el mensaje de error
      }
    } catch (error) {
      console.error('Error para enviar el correo de recuperación', error);
    }
  }; */

  return (
    <View style={styles.formContainer}>
      <Input
        placeholder="Por favor ingresa tu correo..."
        containerStyle={styles.inputForm}
        onChange={(e) => setEmail(e.nativeEvent.text)}
        keyboardType="email-address"
        rightIcon={
          <Icon
            type="material-community"
            name="at"
            iconStyle={styles.Icon}
          />
        }
      />
      <Button
        title="Recuperar Contraseña"
        containerStyle={styles.btnContainer}
        buttonStyle={styles.btnRecover}
        onPress={null}
      />

      <Button
        title="Obtener datos de promotores"
        containerStyle={styles.btnContainer}
        buttonStyle={styles.btnRecover1}
        onPress={null}
      />

      {showMessage && (
        <Text style={styles.messageText}>Verifica tu correo electrónico para restablecer la contraseña.</Text>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30
  },
  inputForm: {
    width: "90%"
  },
  btnContainer: {
    borderWidth: 1,
    borderRadius: 20,
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
    marginBottom: 10,
    
  },
  btnRecover: {
    backgroundColor: "#4caf50",
  },
  btnRecover1: {
    backgroundColor: "#A6CF98",
  },
 
  icon: {
    color: "#c1c1c1"
  },
  messageText: {
    marginTop: 20,
    color: "#442484",
    fontSize: 16,
    textAlign: "center"
  }
});
