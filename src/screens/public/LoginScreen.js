import React, { useState } from "react";
import { View, Text, TextInput, Image, StyleSheet, TouchableOpacity } from "react-native";
import { MaterialIcons } from '@expo/vector-icons';
import axios from 'axios';

export default function LoginScreen(props) {
    const { navigation } = props;

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const [passwordVisible, setPasswordVisible] = useState(false);


    const togglePasswordVisibility = () => {
        setPasswordVisible(!passwordVisible);
    };

    const handleLogin = async () => {
        try {
            // Preparar los datos para enviar al nuevo enlace de inicio de sesión
            const data = {
                correo: email,
                contrasena: password,
            };

            const response = await axios.post('https://casac-com.onrender.com/auth/login', data);

            if (response.status === 200) {
                console.log('Autenticación exitosa');
                setError('');
                navigation.navigate('Main');
            } else {
                const responseData = response.data;
                setError(responseData.message || 'Credenciales incorrectas. Verifica tus datos.');
            }
        } catch (err) {
            console.error('Error de inicio de sesión:', err.message);
            setError('Error de inicio de sesión. Verifica tus credenciales.');
        }
    };


    const goToRecu = () => {
        navigation.navigate("Recu");
    };

    const goToRegistrate = () => {
        navigation.navigate("Registrate");
    };

    return (
        <View style={styles.container}>
            <View style={styles.imageContainer}>
                <Image source={require('../../assets/login.png')} style={styles.logo} />
            </View>


            <View style={styles.formContainer}>
                <Text style={styles.title}>Inicio de sesión</Text>
                <Text style={styles.titulo}>Email</Text>
                <TextInput
                    style={styles.input}
                    placeholder="Ingresa tu email"
                    onChangeText={(text) => setEmail(text)}
                />

                <Text style={styles.titulo}> Contraseña</Text>
                <View style={styles.passwordContainer}>
                    <TextInput
                        style={styles.passwordInput}
                        placeholder="Contraseña"
                        secureTextEntry={!passwordVisible}
                        onChangeText={(text) => setPassword(text)}
                    />


                    <TouchableOpacity onPress={togglePasswordVisibility} style={styles.eyeIcon}>
                        <MaterialIcons name={passwordVisible ? 'visibility-off' : 'visibility'} size={24} color="black" />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={styles.containerLink} onPress={goToRecu}>
                    <Text style={styles.links}> ¿Has olvidado tu contraseña? </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btn} onPress={handleLogin}>
                    <Text style={styles.btnText}>Iniciar Sesión</Text>
                </TouchableOpacity>
                {/*                 <TouchableOpacity>
                    <Text style={styles.links}onPress={goToRegistrate}> Registrarse </Text>
                </TouchableOpacity> */}
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    formContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '115%',
        paddingHorizontal: 20, // Alinea el formulario con el ancho de la pantalla
        marginTop: -100, // Espacio entre la imagen y el formulario
        marginBottom: 2,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 20,

    },
    title: {
        fontSize: 35,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 30,
        padding: 8,
        marginTop: 10,

    },
    input: {
        height: 40,
        width: '100%',
        borderWidth: 1,
        marginBottom: 20,
        paddingHorizontal: 10,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#BBC3BF',

    },
    titulo: {
        alignSelf: 'flex-start', // Coloca el texto a la derecha
        marginRight: 10, // Margen derecho para el texto
        marginBottom: 5, // Espacio entre texto y input
        fontWeight: 'bold', // Para hacer el texto más visible o destacado
    },

    btn: {
        backgroundColor: "#4caf50",
        borderRadius: 17,
        paddingVertical: 10,
        width: "90%",
        height: "8%",
    },
    btnText: {
        color: "white", // Cambia el color del texto en el botón
        fontWeight: "bold",
        textAlign: "center",
        fontSize: 16,
    },

    containerLink: {
        alignSelf: "flex-end",
        marginTop: -20,
        marginBottom: 20,

    },
    links: {
        color: "#4caf50",
        alignSelf: "flex-end",
        marginBottom: 10,
        fontSize: 15,
        marginTop: 8,
    },
    passwordContainer: {
        flexDirection: 'row',
        alignItems: 'center',

        borderWidth: 1,
        borderRadius: 8,
        borderColor: '#BBC3BF',
        marginBottom: 20,
    },
    passwordInput: {
        flex: 1,
        height: 40,
        paddingHorizontal: 10,

    },
    eyeIcon: {
        padding: 10,
    },
    imageContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '112%',
        height: "40%",
        marginTop: -200,
        marginBottom: 0


    },
    logo: {
        width: '100%', // Ocupa la mitad del ancho del contenedor
        height: '60%', // Ocupa la mitad del alto del contenedor
        resizeMode: 'cover',
        marginBottom: -150,
    },


});
