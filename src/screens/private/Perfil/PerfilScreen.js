import React from "react";
import { View, Text, StyleSheet } from "react-native";

const PerfilScreen = () => {
  const porcentajeExito = 80; // Puedes cambiar este valor según tus necesidades

  return (
    <View style={styles.container}>
      <Text style={styles.title}>¡Bienvenido a tu Perfil!</Text>
      <View style={styles.infoContainer}>
        <View style={styles.row}>
          <Text style={styles.label}>Guias Completadas:</Text>
          <Text style={styles.value}>0</Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.label}>Guias Pendientes:</Text>
          <Text style={styles.value}>0</Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.label}>Rol:</Text>
          <Text style={styles.value}>Promotor</Text>
        </View>
      </View>
      <View style={styles.progressContainer}>
        <View
          style={[
            styles.progressBar,
            { width: `${porcentajeExito}%` },
          ]}
        >
          <Text style={styles.progressText}>{`${porcentajeExito}%`}</Text>
        </View>
        <Text style={styles.progressLabel}>Porcentaje de éxito</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#E6FFE6", // Verde claro
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
    color: "#006600", // Verde oscuro
  },
  infoContainer: {
    width: "80%",
    marginBottom: 20,
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 10,
  },
  label: {
    fontSize: 18,
    color: "#009900", // Verde
  },
  value: {
    fontSize: 18,
    color: "#006600", // Verde oscuro
  },
  progressContainer: {
    alignItems: "center",
  },
  progressBar: {
    height: 20,
    backgroundColor: "#99FF99", // Verde claro
    borderRadius: 10,
    overflow: "hidden",
    marginBottom: 10,
  },
  progressText: {
    fontSize: 16,
    color: "#006600", // Verde oscuro
    textAlign: "center",
  },
  progressLabel: {
    fontSize: 16,
    color: "#009900", // Verde
  },
});

export default PerfilScreen;
