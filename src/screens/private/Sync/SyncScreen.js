import React, { useRef, useEffect } from "react";
import { View, Text, Animated, Easing, StyleSheet } from "react-native";

const SyncScreen = () => {
  const fadeAnim = useRef(new Animated.Value(1)).current;
  const progressAnim = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    const fadeInOut = () => {
      Animated.sequence([
        Animated.timing(fadeAnim, {
          toValue: 0,
          duration: 1000,
          useNativeDriver: true,
        }),
        Animated.timing(fadeAnim, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: true,
        }),
      ]).start(() => {
        fadeInOut(); // Llamada recursiva para crear un bucle infinito
      });
    };

    const progressAnimation = () => {
      Animated.timing(progressAnim, {
        toValue: 100,
        duration: 5000, // Duración de la animación en milisegundos
        easing: Easing.linear,
        useNativeDriver: false,
      }).start();
    };

    fadeInOut();
    progressAnimation();
  }, [fadeAnim, progressAnim]);

  return (
    <View style={styles.container}>
      <Animated.View style={[styles.fadeContainer, { opacity: fadeAnim }]}>
        <Text style={styles.title}>CASAC</Text>
        <Text style={styles.text}>Sincronizando, espera un momento...</Text>
      </Animated.View>
      <View style={styles.progressBarContainer}>
        <Animated.View
          style={[
            styles.progressBar,
            { width: progressAnim.interpolate({ inputRange: [0, 100], outputRange: ["0%", "100%"] }) },
          ]}
        />
        <Text style={styles.progressText}>
          {progressAnim._value === 100 ? "Hemos terminado con la sincronización" : `Progreso: ${Math.round(progressAnim._value)}%`}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  fadeContainer: {
    marginBottom: 20,
  },
  title: {
    fontSize: 32,
    fontWeight: "bold",
  },
  text: {
    fontSize: 18,
    textAlign: "center",
  },
  progressBarContainer: {
    width: "80%",
    height: 20,
    backgroundColor: "#ccc",
    borderRadius: 10,
    overflow: "hidden",
    marginVertical: 20,
  },
  progressBar: {
    height: "100%",
    backgroundColor: "green",
  },
  progressText: {
    fontSize: 16,
    textAlign: "center",
    marginTop: 5,
  },
});

export default SyncScreen;
