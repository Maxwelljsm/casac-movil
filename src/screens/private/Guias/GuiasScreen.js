import React, { useEffect, useState } from "react";
import { View, TextInput, TouchableOpacity, Text, ScrollView, Animated, Easing } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import axios from "axios";
import { useNavigation } from "@react-navigation/native";
import { styles } from "../../../styles/Guiasstyles";

const GuiasScreen = () => {
  const navigation = useNavigation();
  const [searchTerm, setSearchTerm] = useState("");
  const [guiaData, setGuiaData] = useState([]);
  const [filteredGuiaData, setFilteredGuiaData] = useState([]);

  const animatedValue = new Animated.Value(0);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get("https://casac-com.onrender.com/guia/listarActivas");
        const guiasActivas = response.data?.guiasActivas;

        if (guiasActivas && Array.isArray(guiasActivas)) {
          setGuiaData(guiasActivas);
          setFilteredGuiaData(guiasActivas);

          // Ajustamos el rango de interpolación para que vaya hasta el 99%
          Animated.timing(animatedValue, {
            toValue: 1,
            duration: 3000,
            easing: Easing.linear,
            useNativeDriver: false,
          }).start();
        } else {
          console.error("La respuesta del servicio no tiene el formato esperado:", response.data);
        }
      } catch (error) {
        console.error("Error al obtener guías activas:", error);
      }
    };

    fetchData();
  }, []); // Se ejecutará solo una vez al montar el componente

  const goToFormScreen = (guiaId) => {
    navigation.navigate('Form', { guiaId });
  };

  const handleSearch = () => {
    const searchTermLowerCase = searchTerm.toLowerCase();
    const filteredData = guiaData.filter((guia) =>
      guia.titulo_guia.toLowerCase().includes(searchTermLowerCase)
    );
    setFilteredGuiaData(filteredData);
  };

  const renderGuias = () => {
    return filteredGuiaData.map((guia) => (
      <TouchableOpacity key={guia.id} style={styles.card} onPress={() => goToFormScreen(guia.id)}>
        <View style={styles.titleContainer}>
          <Icon name="file-document" size={28} style={styles.icon} />
          <Text style={styles.title}>{guia.titulo_guia}</Text>
        </View>
        <View style={styles.progressContainer}>
          <Animated.View
            style={[
              styles.progressBar,
              {
                width: animatedValue.interpolate({
                  inputRange: [0, 1], // Cambiado a [0, 1]
                  outputRange: ["0%", "100%"], // Cambiado a ["0%", "99%"]
                }),
              },
            ]}
          />
          <Text style={styles.progressText}>Avance</Text>
        </View>
        <View style={styles.detailsContainer}>
          <View style={styles.detailsItem}>
            <Icon name="book-open-page-variant" size={20} style={styles.detailsIcon} />
            <Text style={styles.detailsText}>Preguntas: {guia.cantidadPreguntas}</Text>
          </View>
          <View style={styles.detailsItem}>
            <Icon name="comment-multiple" size={20} style={styles.detailsIcon} />
            <Text style={styles.detailsText}>Respuestas: 15</Text>
          </View>
        </View>
      </TouchableOpacity>
    ));
  };

  return (
    <ScrollView style={styles.principal}>
      <View style={styles.container}>
        <View style={styles.searchContainer}>
          <Icon name="magnify" size={24} color="#4caf50" style={styles.searchIcon} />
          <TextInput
            style={styles.input}
            placeholder="Buscar guias..."
            placeholderTextColor="#BBC3BF"
            value={searchTerm}
            onChangeText={(text) => setSearchTerm(text)}
            onEndEditing={handleSearch}
          />
        </View>
        <TouchableOpacity style={styles.cancelButtonContainer}>
          <Text style={styles.linkCan}> Cancelar</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.containerPrincipal}>
        {renderGuias()}
      </View>
    </ScrollView>
  );
};

export default GuiasScreen;
