import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, StyleSheet, ScrollView } from "react-native";
import axios from "axios";
import { MaterialIcons } from '@expo/vector-icons';

const FormScreen = ({ route }) => {
  const guiaId = route.params?.guiaId;
  const [responseData, setResponseData] = useState(null);
  const [currentSessionIndex, setCurrentSessionIndex] = useState(0);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`https://casac-com.onrender.com/sesiones/search_sesions/${guiaId}`);
        setResponseData(response.data);

        // Puedes ver la respuesta en la consola
        console.log(JSON.stringify(response.data));
      } catch (error) {
        console.error("Error al obtener datos:", error);
      }
    };

    fetchData();
  }, [guiaId]);

  const renderQuestionTypeIcon = (tipo) => {
    switch (tipo) {
      case "Lista desplegable":
        return <MaterialIcons name="arrow-drop-down" size={24} color="black" />;
      case "Tipo Pregunta":
        return <MaterialIcons name="check-circle" size={24} color="black" />;
      // Agrega más casos según los tipos de pregunta que puedas tener
      default:
        return null;
    }
  };

  const renderSessionPage = (sessionData, pageIndex) => {
    if (!sessionData || !sessionData.sesiones) {
      return null;
    }
  
    const session = sessionData.sesiones[pageIndex];
  
    if (!session || !session.sesion || !session.sesion.preguntas) {
      return null;
    }
  
    return (
      <View key={pageIndex} style={styles.sessionContainer}>
        <Text style={styles.sessionTitle}>{session.sesion.titulo_sesion}</Text>
  
        {session.sesion.preguntas.map((pregunta, preguntaIndex) => {
          if (!pregunta || !pregunta.respuestas) {
            return null;
          }
  
          return (
            <View key={preguntaIndex} style={styles.questionContainer}>
              <View style={styles.questionHeader}>
                {renderQuestionTypeIcon(pregunta.tipo)}
                <Text style={styles.questionTitle}>{pregunta.titulo_pregunta}</Text>
              </View>
  
              {pregunta.respuestas.map((respuesta, respuestaIndex) => (
                <Text key={respuestaIndex} style={styles.answerText}>
                  {`Respuesta: ${respuesta.respuesta} (${pregunta.tipo})`}
                </Text>
              ))}
            </View>
          );
        })}
      </View>
    );
  };
  
  const renderCarousel = () => {
    if (!responseData || !responseData.sesiones) {
      return null;
    }
  
    return (
      <ScrollView
        horizontal
        pagingEnabled
        showsHorizontalScrollIndicator={false}
        onMomentumScrollEnd={(event) => {
          const pageIndex = Math.round(event.nativeEvent.contentOffset.x / event.nativeEvent.layoutMeasurement.width);
          setCurrentSessionIndex(pageIndex);
        }}
      >
        {responseData.sesiones.map((session, pageIndex) =>
          renderSessionPage({ sesiones: responseData.sesiones }, pageIndex)
        )}
      </ScrollView>
    );
  };

  return (
    <View style={styles.container}>
      <Text style={styles.guiaIdText}>ID de la guía seleccionada: {guiaId}</Text>
      <Text style={styles.responseText}>Respuesta del servicio:</Text>
      {renderCarousel()}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    backgroundColor: "#fff",
  },
  guiaIdText: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 8,
  },
  responseText: {
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 8,
  },
  sessionContainer: {
    width: "100%",
    marginBottom: 16,
  },
  sessionTitle: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 8,
  },
  questionContainer: {
    marginBottom: 12,
  },
  questionHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 4,
  },
  questionTitle: {
    fontSize: 16,
    marginLeft: 8,
  },
  answerText: {
    fontSize: 14,
    marginLeft: 24,
  },
});

export default FormScreen;
