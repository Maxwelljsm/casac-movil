import React from "react";
import { ScrollView, View } from "react-native";
import TablaComponent from "../../../components/TablaComponent";
import { useNavigation } from "@react-navigation/native";
import axios from "axios";
import { styles } from "../../../styles/Guiasstyles";

const RespuestasScreen = () => {
  const navigation = useNavigation();

  const fetchData = async () => {
    try {
      const response = await axios.get("https://casac-com.onrender.com/guia/listarActivas");
      return response;
    } catch (error) {
      console.error("Error al obtener guías activas:", error);
      throw error;
    }
  };

  const goToConsultarPreguntas = () => {
    navigation.navigate("ConsultarPreguntas");
  };

  return (
    <ScrollView style={styles.principal}>
      <View style={styles.containerPrincipal}>
        <TablaComponent fetchData={fetchData} />
      </View>
    </ScrollView>
  );
};

export default RespuestasScreen;
