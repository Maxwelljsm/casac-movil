import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  principal: {
    backgroundColor: "white",
  },
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: 20,
    backgroundColor: "#4caf89",
    flexDirection: 'row',
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 5,
    paddingHorizontal: 10,
    marginBottom: 20,
    width: "80%",
    backgroundColor: "white",
  },
  input: {
    flex: 1,
    height: 40,
    fontSize: 16,
    marginLeft: 5,
    backgroundColor: "white",
  },
  searchIcon: {
    marginRight: 5,
  },
  linkCan: {
    textAlign: "center",
    color: "white",
    fontSize: 16,
  },
  cancelButtonContainer: {
    marginLeft: 5.5,
    paddingTop: 10,
  },

  containerPrincipal: {
    margin: 10,
  },

  head: { height: 40, backgroundColor: "#f1f8ff" },
  headText: { textAlign: "center", fontWeight: "bold" },
  text: { textAlign: "center" },

  card: {
    backgroundColor: "#fff",
    borderRadius: 8,
    padding: 20,
    margin: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginTop: 15,
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 10,
    color: "#4caf89",
  },
  description: {
    fontSize: 14,
    marginBottom: 10,
    textAlign: "center",
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  icon: {
    marginRight: 18,
    color: "#4caf89",
  },
  progressContainer: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginBottom: 10,
  },
  progressBar: {
    height: 10,
    backgroundColor: "#2ecc71", // Verde para la barra de progreso
    borderRadius: 5,
  },
  progressText: {
    fontSize: 14,
    color: "#4caf89", // Verde oscuro para el texto de progreso
  },

  detailsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  detailsItem: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  detailsIcon: {
    marginRight: 5,
    color: "#4caf89", // Verde oscuro para los iconos de detalles
  },
  detailsText: {
    fontSize: 14,
    color: "#000", // Verde oscuro para el texto de detalles
    marginLeft: 5,
  },
});
