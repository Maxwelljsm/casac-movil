import React, { useEffect, useState } from "react";
import { View, Text } from "react-native";
import { Table, Row } from "react-native-table-component";
import axios from "axios";
import { styles } from "../styles/Guiasstyles";

const TablaComponent = ({ fetchData }) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchAndSetData = async () => {
      try {
        const response = await fetchData();
        const guiasData = response.data?.guiasActivas;

        if (guiasData && Array.isArray(guiasData)) {
          setData(guiasData);
        } else {
          console.error("La respuesta del servicio no tiene el formato esperado:", response.data);
        }
      } catch (error) {
        console.error("Error al obtener datos:", error);
      }
    };

    fetchAndSetData();
  }, [fetchData]);

  return (
    <Table borderStyle={{ borderWidth: 1, borderColor: "#f1f8ff" }}>
      <Row
        data={["Título", "Fecha Creación", "Preguntas", "Respuestas"]}
        style={styles.head}
        textStyle={styles.headText}
      />
      {data.length > 0 ? (
        data.map((guia) => (
          <Row
            key={guia.id}
            data={[
              guia.titulo_guia,
              guia.fecha_creacion,
              guia.cantidadPreguntas,
              "0", // Puedes reemplazar esto con la cantidad real de respuestas
            ]}
            textStyle={styles.text}
          />
        ))
      ) : (
        <Text>No hay guías disponibles.</Text>
      )}
    </Table>
  );
};

export default TablaComponent;
