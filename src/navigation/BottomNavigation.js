import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import GuiasStack from "./GuiasStack"
import SyncScreen from "../screens/private/Sync/SyncScreen";
import PerfilScreen from "../screens/private/Perfil/PerfilScreen";
import RespuestasScreen from "../screens/private/Respuestas/RespuestasScreen";
import { MaterialIcons } from '@expo/vector-icons';

const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => (
  <Tab.Navigator
    initialRouteName="Guias"
    screenOptions={({ route }) => ({
      tabBarLabel: route.name,
      tabBarIcon: ({ color, size }) => {
        let iconName;

        if (route.name === "Guias") {
          iconName = "book";
        } else if (route.name === "Sync") {
          iconName = "sync";
        } else if (route.name === "Respuestas") {
          iconName = "message";
        } else if (route.name === "Perfil") {
          iconName = "person";
        }

        return <MaterialIcons name={iconName} color={color} size={size} />;
      },
    })}
    tabBarOptions={{
      activeTintColor: "#2ecc71",
      inactiveTintColor: "#95a5a6",
      style: {
        display: "flex",
      },
    }}
  >
    <Tab.Screen
      name="Guias"
      component={GuiasStack}
      options={{ headerShown: false }}
    />
    <Tab.Screen
      name="Sync"
      component={SyncScreen}
      options={{ headerShown: false }}
    />
    <Tab.Screen
      name="Respuestas"
      component={RespuestasScreen}
      options={{ headerShown: false }}
    />
    <Tab.Screen
      name="Perfil"
      component={PerfilScreen}
      options={{ headerShown: false }}
    />
  </Tab.Navigator>
);

export default BottomTabNavigator;
