import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import GuiasScreen from "../screens/private/Guias/GuiasScreen";
import FormScreen from "../screens/private/Guias/FormScreen";

const Stack = createStackNavigator();

const GuiasStack = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="GuiasDesp"
      component={GuiasScreen}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name="Form"
      component={FormScreen}
      options={{ headerShown: false }}
    />
  </Stack.Navigator>
);

export default GuiasStack;