import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import LoginScreen from "../screens/public/LoginScreen";
import BottomTabNavigator from "./BottomNavigation";
import FormScreen from "../screens/private/Guias/FormScreen" 
const Stack = createStackNavigator();

const AppNavigator = () =>  {
    return (
        <Stack.Navigator initialRouteName="Login">
          <Stack.Screen
            name="Login"
            component={LoginScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Main"
            component={BottomTabNavigator}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Form"
            component={FormScreen}
            options={{ headerShown: false }}
          />
        </Stack.Navigator>
      );
}

export default AppNavigator;